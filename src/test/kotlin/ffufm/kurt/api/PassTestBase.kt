package ffufm.kurt.api

import com.fasterxml.jackson.databind.ObjectMapper
import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.security.SpringSecurityAuditorAware
import ffufm.kurt.api.repositories.project.ProjectProjectRepositories
import ffufm.kurt.api.repositories.task.TaskTaskRepository
import ffufm.kurt.api.repositories.user.UserAddressRepository
import ffufm.kurt.api.repositories.user.UserContactDetailRepository
import ffufm.kurt.api.repositories.user.UserUserRepository
import ffufm.kurt.api.spec.handler.project.ProjectProjectDatabaseHandler
import ffufm.kurt.api.spec.handler.task.TaskTaskDatabaseHandler
import ffufm.kurt.api.spec.handler.user.UserAddressDatabaseHandler
import ffufm.kurt.api.spec.handler.user.UserContactDetailDatabaseHandler
import ffufm.kurt.api.spec.handler.user.UserUserDatabaseHandler
import org.junit.After
import org.junit.Before
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationContext
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc

@RunWith(SpringRunner::class)
@ActiveProfiles("test")
@SpringBootTest(classes = [SBKurt::class, SpringSecurityAuditorAware::class])
@AutoConfigureMockMvc
abstract class PassTestBase {
    @Autowired
    lateinit var context: ApplicationContext

    @Autowired
    lateinit var userUserRepository: UserUserRepository

    @Autowired
    lateinit var userContactDetailRepository: UserContactDetailRepository

    @Autowired
    lateinit var userAddressRepository: UserAddressRepository

    @Autowired
    lateinit var projectProjectRepositories: ProjectProjectRepositories

    @Autowired
    lateinit var taskTaskRepository: TaskTaskRepository

    @Autowired
    lateinit var taskTaskDatabaseHandler: TaskTaskDatabaseHandler

    @Autowired
    lateinit var userUserDatabaseHandler: UserUserDatabaseHandler

    @Autowired
    lateinit var userContactDetailDatabaseHandler: UserContactDetailDatabaseHandler

    @Autowired
    lateinit var userAddressDatabaseHandler: UserAddressDatabaseHandler

    @Autowired
    lateinit var projectProjectDatabaseHandler: ProjectProjectDatabaseHandler


    @After
    fun cleanRepositories() {
        userUserRepository.deleteAll()
        userContactDetailRepository.deleteAll()
        userAddressRepository.deleteAll()
        projectProjectRepositories.deleteAll()
        taskTaskRepository.deleteAll()
    }

    @Autowired
    lateinit var objectMapper: ObjectMapper

    @Autowired
    lateinit var mockMvc: MockMvc


    @Before
    fun initializeContext() {
        SpringContext.context = context
    }
}
