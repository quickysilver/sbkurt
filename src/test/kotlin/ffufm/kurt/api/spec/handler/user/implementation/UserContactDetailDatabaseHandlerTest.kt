package ffufm.kurt.api.spec.handler.user.implementation

import ffufm.kurt.api.PassTestBase
import ffufm.kurt.api.spec.dbo.user.UserContactDetail
import ffufm.kurt.api.spec.handler.user.UserContactDetailDatabaseHandler
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired

class UserContactDetailDatabaseHandlerTest : PassTestBase() {
    @Before
    @After


    @Test
    fun `test should return contact detail`() = runBlocking {
        val body: UserContactDetail = UserContactDetail()
        val id: Long = 0
        userContactDetailDatabaseHandler.createContactDetail(body.toDto(), id)
        Unit
    }

    @Test
    fun `remove contact detail should return`() = runBlocking {
        val id: Long = 0
        userContactDetailDatabaseHandler.remove(id)
        Unit
    }
}
