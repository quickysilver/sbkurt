package ffufm.kurt.api.spec.handler.user.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.kurt.api.PassTestBase
import ffufm.kurt.api.repositories.user.UserContactDetailRepository
import ffufm.kurt.api.spec.dbo.user.UserContactDetail
import ffufm.kurt.api.spec.dbo.user.UserUser
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class UserContactDetailHandlerTest : PassTestBase() {
    @Test
    @WithMockUser
    fun `test createContactDetail`() {
        val body: UserContactDetail = UserContactDetail()
        val id: Long = 0
                mockMvc.post("/users/{id}/contact-details/", id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(body)
                }.andExpect {
                    status { isOk() }

                }
    }

    @Test
    @WithMockUser
    fun `test remove`() {
        val id: Long = 0
                mockMvc.delete("/users/contact-details/{id}", id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON

                }.andExpect {
                    status { isOk() }

                }
    }
}
