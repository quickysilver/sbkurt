package ffufm.kurt.api.spec.handler.user.utils

import ffufm.kurt.api.spec.dbo.project.ProjectProject
import ffufm.kurt.api.spec.dbo.task.TaskTask
import ffufm.kurt.api.spec.dbo.user.UserUser

object EntityGenerator {
    fun createUser(): UserUser = UserUser(
        firstName = "Brandon",
        lastName = "Cruz",
        email = "brandon@brandon.com",
        userType = "GC"
    )
    fun createProject(): ProjectProject = ProjectProject(
        name = "Company A Project",
        description = "For the new room",
        status = "ON-GOING"
    )
    fun createTask() : TaskTask = TaskTask(
        name = "Task 1",
        description = "For the new project",
        status = "ON-GOING",
        project = createProject()
    )
}