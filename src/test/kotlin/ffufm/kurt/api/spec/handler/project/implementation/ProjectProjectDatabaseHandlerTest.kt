package ffufm.kurt.api.spec.handler.project.implementation

import ffufm.kurt.api.PassTestBase
import ffufm.kurt.api.spec.dbo.user.UserUser
import ffufm.kurt.api.spec.handler.project.ProjectProjectDatabaseHandler
import ffufm.kurt.api.spec.handler.user.UserUserDatabaseHandler
import ffufm.kurt.api.spec.handler.user.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import kotlin.test.assertEquals

class ProjectProjectDatabaseHandlerTest: PassTestBase() {
    @Test
    fun `create should return project`() = runBlocking {
        assertEquals(0, projectProjectRepositories.findAll().count())
        val user = EntityGenerator.createUser()
        val createdUser = userUserDatabaseHandler.create(user.toDto()).toEntity()
        val project = EntityGenerator.createProject()
        val createdProject = projectProjectDatabaseHandler.create(project.toDto(), createdUser.id!!)
        assertEquals(1, userUserRepository.findAll().count())

        Assertions.assertThat(createdProject).usingRecursiveComparison().ignoringFields(
            UserUser:: id.name
        ).withStrictTypeChecking().isEqualTo(project)

    }


    @Test
    fun `test update`() = runBlocking {
        val project = EntityGenerator.createProject()
        val original = projectProjectRepositories.save(project)
        val body = original.copy(
            name = "NEW Company A Project",
            description = "For the new rooms",
            status = "ON-GOING"
        )
        val updatedProject = projectProjectDatabaseHandler.update(body.toDto(), original.id!!)
        assertEquals(body.description, updatedProject.description)
    }
    @Test
    fun `test getAll`() = runBlocking {

        val body = EntityGenerator.createProject()
        projectProjectRepositories.saveAll(
            listOf(
                body,
                body.copy(name = "NEW Company A Project")
            )
        )
        val projects = projectProjectDatabaseHandler.getAll(0, 100)
        assertEquals(2, projectProjectRepositories.findAll().count() )
    }
}