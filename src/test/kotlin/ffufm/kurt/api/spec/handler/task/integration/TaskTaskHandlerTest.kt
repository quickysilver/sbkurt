package ffufm.kurt.api.spec.handler.task.integration

import ffufm.kurt.api.PassTestBase
import ffufm.kurt.api.spec.handler.user.utils.EntityGenerator
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class TaskTaskHandlerTest: PassTestBase() {
    @Test
    @WithMockUser
    fun `createTask should return isOk`() {
        val owner = userUserRepository.save(EntityGenerator.createUser())
        val project = projectProjectRepositories.save(
            EntityGenerator.createProject().copy(owner = owner)
        )

        val body = EntityGenerator.createTask().copy(
            assignee = owner,
            project = project )

        mockMvc.post(
            "/users/{userId}/projects/{projectId}/tasks/",
            project.id!!, owner.id!!) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.asyncDispatch().andExpect {
            status { isOk() }
        }
    }

    @Test
    @WithMockUser
    fun `createTask should return invalid userId`() {
        val owner = userUserRepository.save(EntityGenerator.createUser())
        val project = projectProjectRepositories.save(
            EntityGenerator.createProject().copy(
                owner = owner
            )
        )

        val invalidId : Long = 123

        val body = EntityGenerator.createTask().copy(
            assignee = owner,
            project = project )

        mockMvc.post(
            "/users/{userId}/projects/{projectId}/tasks/",
            project.id!!, invalidId) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.asyncDispatch().andExpect {
            status { isNotFound() }
        }
    }

    @Test
    @WithMockUser
    fun `update should return isOk`() {
        val owner = userUserRepository.save(EntityGenerator.createUser())
        val project = projectProjectRepositories.save(
            EntityGenerator.createProject().copy( owner = owner ))

        val task = taskTaskRepository.save(EntityGenerator.createTask().copy(
            assignee = owner,
            project = project ))

        val body = task.copy(
            status = "ON-GOING"
        )

        val invalidId : Long  = 123

        mockMvc.put(
            "/tasks/{id}/", task.id!!  ) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.asyncDispatch().andExpect {
            status { isOk() }
        }
    }

    @Test
    @WithMockUser
    fun `remove should return isOk`() {
        val owner = userUserRepository.save(EntityGenerator.createUser())
        val project = projectProjectRepositories.save(
            EntityGenerator.createProject().copy( owner = owner ))

        val task = taskTaskRepository.save(EntityGenerator.createTask().copy(
            assignee = owner,
            project = project
        ))
        mockMvc.delete("/tasks/{id}/", task.id) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.asyncDispatch().andExpect {
            status { isOk() }
        }
    }
}