package ffufm.kurt.api.spec.handler.task.implementation

import ffufm.kurt.api.PassTestBase
import ffufm.kurt.api.spec.handler.user.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import org.springframework.web.server.ResponseStatusException
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class TaskTaskDatabaseHandlerTest: PassTestBase() {
    @Test
    fun `create should return task`() = runBlocking{
        assertEquals(0, taskTaskRepository.findAll().count())
        val owner = userUserRepository.save(EntityGenerator.createUser())
        val project = projectProjectRepositories.save(EntityGenerator.createProject().copy(owner = owner))

        val body = EntityGenerator.createTask().toDto()
        val createdTask = taskTaskDatabaseHandler.createTasks(body, project.id!!, owner.id!!)

        assertEquals(1, taskTaskRepository.findAll().count())
        assertEquals(body.name, createdTask.name)
        assertEquals(body.description, createdTask.description)
        assertEquals(body.status, createdTask.status)
    }
    @Test
    fun `create should fail using invalid userId`() = runBlocking {
        val owner = userUserRepository.save(EntityGenerator.createUser())
        val project = projectProjectRepositories.save(
            EntityGenerator.createProject().copy(owner = owner))

        val invalidId : Long = 123
        val body = EntityGenerator.createTask().toDto()

        val exception = assertFailsWith<ResponseStatusException> {
            taskTaskDatabaseHandler.createTasks(body, project.id!!, invalidId )
        }
        val expectedException = "404 NOT_FOUND \"User with Id $invalidId was not found\""
        assertEquals(expectedException, exception.message)
    }
    @Test
    fun `create should fail given invalid projectId`() = runBlocking {
        val owner = userUserRepository.save(EntityGenerator.createUser())
        projectProjectRepositories.save(EntityGenerator.createProject().copy(owner = owner))

        val invalidId : Long  = 123
        val body = EntityGenerator.createTask().toDto()

        val exception = assertFailsWith<ResponseStatusException> {
            taskTaskDatabaseHandler.createTasks(body, invalidId, owner.id!! )
        }
        val expectedException = "404 NOT_FOUND \"Project with Id $invalidId was not found\""
        assertEquals(expectedException, exception.message)
    }
    @Test
    fun `update should return updated task`() = runBlocking {
        val task = EntityGenerator.createTask()
        val original = taskTaskRepository.save(task)
        val body = original.copy(
            name = "Task 2").toDto()

        val updatedTask = taskTaskDatabaseHandler.update(
            body, original.id!!
        )
        assertEquals(body.name, updatedTask.name)
    }
    @Test
    fun `delete task should return`() = runBlocking {
        val task = EntityGenerator.createTask()
        val createdTask = taskTaskRepository.save(task)
        assertEquals(1, taskTaskRepository.findAll().count())
        taskTaskDatabaseHandler.remove(createdTask.id!!)
        assertEquals(0, taskTaskRepository.findAll().count())
    }



}