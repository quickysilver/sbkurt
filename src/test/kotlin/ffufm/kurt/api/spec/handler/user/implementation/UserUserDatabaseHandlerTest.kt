package ffufm.kurt.api.spec.handler.user.implementation

import ffufm.kurt.api.PassTestBase
import ffufm.kurt.api.spec.dbo.user.UserUser
import ffufm.kurt.api.spec.handler.user.UserUserDatabaseHandler
import ffufm.kurt.api.spec.handler.user.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import kotlin.test.assertEquals

class UserUserDatabaseHandlerTest : PassTestBase() {
    @Test
    fun `getById should return a user`() = runBlocking {
        val user = EntityGenerator.createUser()
        val savedUser = userUserRepository.save(user)

        userUserDatabaseHandler.getById(savedUser.id!!)

        assertEquals(1, userUserRepository.findAll().size)
    }

    @Test
    fun `test getAll`() = runBlocking {

        val body = EntityGenerator.createUser()
        userUserRepository.saveAll(
            listOf(
                body,
                body.copy(email = "brenda@brenda.com")
        )
        )
        val users = userUserDatabaseHandler.getAll(0, 100)
        assertEquals(2, userUserRepository.findAll().count() )
    }

    @Test
    fun `create should return user`() = runBlocking {
        assertEquals(0, userUserRepository.findAll().count())
        val user = EntityGenerator.createUser()
        val createdUser = userUserDatabaseHandler.create(user.toDto())
        assertEquals(1, userUserRepository.findAll().count())

        Assertions.assertThat(createdUser).usingRecursiveComparison().ignoringFields(
            UserUser:: id.name
        ).withStrictTypeChecking().isEqualTo(user)

    }

    @Test
    fun `test update`() = runBlocking {
        val user = EntityGenerator.createUser()
        val original = userUserRepository.save(user)
        val body = original.copy(
            firstName = "Brenda",
            lastName =  "Mage",
            email = "brenda@brenda.com"
        )
        val updatedUser = userUserDatabaseHandler.update(body.toDto(), original.id!!)
        assertEquals(body.firstName, updatedUser.firstName )
        assertEquals(body.lastName, updatedUser.lastName)
    }

    @Test
    fun `test remove`() = runBlocking {
        val user = EntityGenerator.createUser()
        val createdUser = userUserRepository.save(user)

        assertEquals(1, userUserRepository.findAll().count())
        userUserDatabaseHandler.remove(createdUser.id!!)
        assertEquals(0, userUserRepository.findAll().count())
    }
}
