package ffufm.kurt.api.spec.handler.user.implementation

import ffufm.kurt.api.PassTestBase
import ffufm.kurt.api.spec.dbo.user.UserAddress
import ffufm.kurt.api.spec.handler.user.UserAddressDatabaseHandler
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired

class UserAddressDatabaseHandlerTest : PassTestBase() {
    @Test
    fun `create should return address`() = runBlocking {
        val body: UserAddress = UserAddress()
        val id: Long = 0
        userAddressDatabaseHandler.createAddress(body.toDto(), id)
        Unit
    }

    @Test
    fun `update should return updated address`() = runBlocking {
        val body: UserAddress = UserAddress()
        val id: Long = 0
        userAddressDatabaseHandler.updateAddress(body.toDto(), id)
        Unit
    }

    @Test
    fun `remove address should return`() = runBlocking {
        val id: Long = 0
        userAddressDatabaseHandler.remove(id)
        Unit
    }
}
