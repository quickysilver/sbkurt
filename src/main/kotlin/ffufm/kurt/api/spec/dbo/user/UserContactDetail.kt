package ffufm.kurt.api.spec.dbo.user

import am.ik.yavi.builder.ValidatorBuilder
import am.ik.yavi.builder.konstraint
import am.ik.yavi.builder.konstraintOnObject
import de.ffuf.pass.common.models.PassDTO
import de.ffuf.pass.common.models.PassDTOModel
import de.ffuf.pass.common.models.PassDtoSerializer
import de.ffuf.pass.common.models.PassModelValidation
import de.ffuf.pass.common.models.idDto
import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.utilities.extensions.konstraint
import de.ffuf.pass.common.utilities.extensions.toEntities
import de.ffuf.pass.common.utilities.extensions.toSafeDtos
import ffufm.kurt.api.spec.dbo.user.UserUserSerializer
import java.util.TreeSet
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Index
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import kotlin.Boolean
import kotlin.Long
import kotlin.String
import kotlin.reflect.KClass
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.ColumnDefault
import org.hibernate.annotations.FetchMode
import org.springframework.beans.factory.getBeansOfType
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

/**
 * Contact details for user
 */
@Entity(name = "UserContactDetail")
@Table(name = "user_contactdetail")
data class UserContactDetail(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    /**
     * contact details for Contact Detail
     * Sample: 09182575936
     */
    @Column(
        length = 50,
        updatable = true,
        nullable = false,
        name = "contact_details"
    )
    val contactDetails: String = "",
    /**
     * contact type for Contact Detail
     * Sample: PhoneNumber
     */
    @Column(
        length = 50,
        updatable = true,
        nullable = false,
        name = "contact_type"
    )
    val contactType: String = "",
    /**
     * isPrimary true/ false for Contact Detail
     * Sample: TRUE
     */
    @ColumnDefault("TRUE")
    @Column(name = "is_primary")
    val isPrimary: Boolean = false,
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(nullable = false)
    val user: UserUser? = null
) : PassDTOModel<UserContactDetail, UserContactDetailDTO, Long>() {
    override fun toDto(): UserContactDetailDTO =
            super.toDtoInternal(UserContactDetailSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<UserContactDetail, UserContactDetailDTO, Long>,
            UserContactDetailDTO, Long>>)

    override fun readId(): Long? = this.id

    override fun toString(): String = super.toString()
}

/**
 * Contact details for user
 */
data class UserContactDetailDTO(
    val id: Long? = null,
    /**
     * contact details for Contact Detail
     * Sample: 09182575936
     */
    val contactDetails: String? = "",
    /**
     * contact type for Contact Detail
     * Sample: PhoneNumber
     */
    val contactType: String? = "",
    /**
     * isPrimary true/ false for Contact Detail
     * Sample: TRUE
     */
    val isPrimary: Boolean? = false,
    val user: UserUserDTO? = null
) : PassDTO<UserContactDetail, Long>() {
    override fun toEntity(): UserContactDetail =
            super.toEntityInternal(UserContactDetailSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<UserContactDetail, PassDTO<UserContactDetail,
            Long>, Long>, PassDTO<UserContactDetail, Long>, Long>>)

    override fun readId(): Long? = this.id
}

@Component
class UserContactDetailSerializer : PassDtoSerializer<UserContactDetail, UserContactDetailDTO,
        Long>() {
    override fun toDto(entity: UserContactDetail): UserContactDetailDTO = cycle(entity) {
        UserContactDetailDTO(
                id = entity.id,
        contactDetails = entity.contactDetails,
        contactType = entity.contactType,
        isPrimary = entity.isPrimary,
        user = entity.user?.idDto() ?: entity.user?.toDto()
                )}

    override fun toEntity(dto: UserContactDetailDTO): UserContactDetail = UserContactDetail(
            id = dto.id,
    contactDetails = dto.contactDetails ?: "",
    contactType = dto.contactType ?: "",
    isPrimary = dto.isPrimary ?: false,
    user = dto.user?.toEntity()
            )
    override fun idDto(id: Long): UserContactDetailDTO = UserContactDetailDTO(
            id = id,
    contactDetails = null,
    contactType = null,
    isPrimary = null,

            )}

@Service("user.UserContactDetailValidator")
class UserContactDetailValidator : PassModelValidation<UserContactDetail> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<UserContactDetail>):
            ValidatorBuilder<UserContactDetail> = validatorBuilder.apply {
        konstraint(UserContactDetail::contactDetails) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(50)
        }
        konstraint(UserContactDetail::contactType) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(50)
        }
        konstraintOnObject(UserContactDetail::user) {
            notNull()
        }
    }
}

@Service("user.UserContactDetailDTOValidator")
class UserContactDetailDTOValidator : PassModelValidation<UserContactDetailDTO> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<UserContactDetailDTO>):
            ValidatorBuilder<UserContactDetailDTO> = validatorBuilder.apply {
        konstraint(UserContactDetailDTO::contactDetails) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(50)
        }
        konstraint(UserContactDetailDTO::contactType) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(50)
        }
        konstraintOnObject(UserContactDetailDTO::user) {
            notNull()
        }
    }
}
