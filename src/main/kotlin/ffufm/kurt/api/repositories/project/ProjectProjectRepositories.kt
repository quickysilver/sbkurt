package ffufm.kurt.api.repositories.project

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.kurt.api.spec.dbo.project.ProjectProject
import org.springframework.stereotype.Repository

@Repository
interface ProjectProjectRepositories:PassRepository<ProjectProject, Long> {
}