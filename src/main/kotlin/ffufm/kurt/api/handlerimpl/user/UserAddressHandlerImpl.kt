package ffufm.kurt.api.handlerimpl.user

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffufm.kurt.api.repositories.user.UserAddressRepository
import ffufm.kurt.api.spec.dbo.user.UserAddress
import ffufm.kurt.api.spec.dbo.user.UserAddressDTO
import ffufm.kurt.api.spec.handler.user.UserAddressDatabaseHandler
import kotlin.Long
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException

@Component("user.UserAddressHandler")
class UserAddressHandlerImpl : PassDatabaseHandler<UserAddress, UserAddressRepository>(),
        UserAddressDatabaseHandler {
    /**
     * : 
     * HTTP Code 200: Successfully added Address
     */
    override suspend fun createAddress(body: UserAddressDTO, id: Long): UserAddressDTO {
        val bodyEntity = body.toEntity()

        if(repository.doesAddressExist(bodyEntity.street, bodyEntity.barangay, bodyEntity.city, bodyEntity.province, bodyEntity.zipCode)){
            throw ResponseStatusException(HttpStatus.CONFLICT, "Address ${bodyEntity.street} ${bodyEntity.barangay} ${bodyEntity.city} ${bodyEntity.province} ${bodyEntity.zipCode} already exist!")
        }
        return repository.save(bodyEntity).toDto()
    }

    /**
     * : 
     * HTTP Code 200: Successfully updated Address
     */
    override suspend fun updateAddress(body: UserAddressDTO, id: Long): UserAddressDTO {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.save(original).toDto()
    }

    /**
     * : 
     * HTTP Code 200: Successfully deleted Address
     */
    override suspend fun remove(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.delete(original)
    }
}
