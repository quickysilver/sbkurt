package ffufm.kurt.api.handlerimpl.user

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffufm.kurt.api.repositories.user.UserContactDetailRepository
import ffufm.kurt.api.spec.dbo.user.UserContactDetail
import ffufm.kurt.api.spec.dbo.user.UserContactDetailDTO
import ffufm.kurt.api.spec.dbo.user.UserUserDTO
import ffufm.kurt.api.spec.handler.user.UserContactDetailDatabaseHandler
import ffufm.kurt.api.utils.Constants
import kotlin.Long
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException

@Component("user.UserContactDetailHandler")
class UserContactDetailHandlerImpl : PassDatabaseHandler<UserContactDetail,
        UserContactDetailRepository>(), UserContactDetailDatabaseHandler {
    /**
     * : 
     * HTTP Code 200: Successfully added Contact Detail
     */
    override suspend fun createContactDetail(body: UserContactDetailDTO, id: Long): UserContactDetailDTO {
        val countDetailCount = repository.countContactDetailsByUserId(id)
        if(countDetailCount >= Constants.MAX_CONTACT_DETAILS){
            throw ResponseStatusException(HttpStatus.BAD_REQUEST,"You cannot create more than four contact details.")
        }
        return repository.save(body.toEntity()).toDto()
    }




    /**
     * : 
     * HTTP Code 200: Successfully deleted the contact detail
     */
    override suspend fun remove(id: Long){
        val original = repository.findById(id).orElseThrow404(id)
        return repository.delete(original)
    }

    override suspend fun updateContactDetail(body: UserContactDetailDTO, id: Long): UserContactDetailDTO {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.save(original).toDto()
    }
}
