package ffufm.kurt.api.handlerimpl.project

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import de.ffuf.pass.common.utilities.extensions.toDtos
import ffufm.kurt.api.repositories.project.ProjectProjectRepositories
import ffufm.kurt.api.spec.dbo.project.ProjectProject
import ffufm.kurt.api.spec.dbo.project.ProjectProjectDTO
import ffufm.kurt.api.spec.handler.project.ProjectProjectDatabaseHandler
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Component


@Component("project.ProjectProjectHandler")
class ProjectProjectHandlerImpl: PassDatabaseHandler<ProjectProject, ProjectProjectRepositories> (),
ProjectProjectDatabaseHandler{
    override suspend fun create(body: ProjectProjectDTO, id: Long): ProjectProjectDTO {
        val user = repository.findById(id).orElseThrow404(id)
        val bodyEntity = body.toEntity()
        return repository.save(bodyEntity).toDto()
    }
    override suspend fun getAll(maxResults: Int, page: Int): Page<ProjectProjectDTO> {
        return repository.findAll(Pageable.unpaged()).toDtos()
    }

    override suspend fun update(body: ProjectProjectDTO, id: Long): ProjectProjectDTO {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.save(original).toDto()
    }

}