package ffufm.kurt.api.handlerimpl.task

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffufm.kurt.api.repositories.project.ProjectProjectRepositories
import ffufm.kurt.api.repositories.task.TaskTaskRepository
import ffufm.kurt.api.repositories.user.UserUserRepository
import ffufm.kurt.api.spec.dbo.task.TaskTask
import ffufm.kurt.api.spec.dbo.task.TaskTaskDTO
import ffufm.kurt.api.spec.handler.task.TaskTaskDatabaseHandler
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component

@Component("task.TaskTaskHandler")
class TaskTaskHandlerImpl(
    private val userRepository: UserUserRepository,
    private val projectRepository: ProjectProjectRepositories
): PassDatabaseHandler<TaskTask, TaskTaskRepository>(),
TaskTaskDatabaseHandler{
    override suspend fun createTasks(body: TaskTaskDTO, projectId: Long, userId: Long): TaskTaskDTO {
        val bodyEntity = body.toEntity()
        val assignee = userRepository.findById(userId).orElseThrow404(userId)
        val project = projectRepository.findById(projectId).orElseThrow404(projectId)

        return repository.save(bodyEntity.copy(
            assignee = assignee,
            project = project
        )).toDto()
    }

    override suspend fun remove(id: Long) {
        TODO("Not yet implemented")

    }

    override suspend fun update(body: TaskTaskDTO, id: Long): TaskTaskDTO {
        TODO("Not yet implemented")

    }
}